Pre-requests: 
1) java installation
2) curl installation
3) Git bash (Only for windows)



1) Clone this project using above url

2) cd idn-bash-commands-jar

3) java -jar "idn.jar" (Application will start on port number 1086)

4) Open another git bash terminal 
    a) Create account : ./idn signup -fn firstname -ln lastname -email email -password password
    b) Login to account: ./idn login -u email -p password
    c) Create tenant : ./idn create-tenant -t tenantname (this command will generate Tenant Id, Store that id for future reference)
    d) Create Client: ./idn crate-client -cn clientname -cs clientsecret -t tenantId (this command will generate Client Id, Store that Id for future reference)